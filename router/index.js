const express = require('express');
const router = express.Router();
const body = require('body-parser');
router.get('/', (req,res)=>{

    res.render('index.ejs')
    // res.send("<h1> Iniciamos con express </h1>");
    
    })
router.get('/index', (req,res) =>{
    const valores = {
        nRecibo:req.query.nRecibo,
        Nombre:req.query.Nombre,
        Domicilio:req.query.Domicilio,
        tServicio:req.query.tServicio,
        consumido:req.query.consumido
    }
    res.render('index.html',valores);
})

router.post("/index", (req,res) =>{
    const valores = {
        nRecibo:req.body.nRecibo,
        Nombre:req.body.Nombre,
        Domicilio:req.body.Domicilio,
        tServicio:req.body.tServicio,
        consumido:req.body.consumido
    }
    res.render('resultado.html',valores);
})
router.get('/resultado', (req,res) =>{
    const valores = {
        nRecibo:req.query.nRecibo,
        Nombre:req.query.Nombre,
        Domicilio:req.query.Domicilio,
        tServicio:req.query.tServicio,
        consumido:req.query.consumido
    }
    res.render('resultado.html',valores);
})

router.post("/resultado.html", (req,res) =>{
    const valores = {
        nRecibo:req.body.nRecibo,
        Nombre:req.body.Nombre,
        Domicilio:req.body.Domicilio,
        tServicio:req.body.tServicio,
        consumido:req.body.consumido
    }
    res.render('resultado.html',valores);
})

module.exports=router;